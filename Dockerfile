FROM python:3.6

RUN apt update
RUN apt upgrade -y

COPY ${PWD}/install.sh .
COPY ${PWD}/.zshrc /root/.zshrc.bkp
COPY ${PWD}/.vimrc /root/.vimrc
COPY ${PWD}/.zpreztorc /root/.zpreztorc.bkp
COPY ${PWD}/locale /etc/default/locale

RUN pip install flake8
RUN sh ./install.sh
