echo 'updating and upgrading...'
apt update
apt upgrade -y

echo 'setting up locales...'
apt install locales -y
locale-gen en_US.UTF-8

echo 'installing zsh and prezto...'
apt install zsh -y
zsh

echo 'installing prezto...'
git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
mv /root/.zshrc.bkp /root/.zshrc 
mv /root/.zpreztorc.bkp /root/.zpreztorc
chsh -s /bin/zsh

echo 'setting up zsh-completions...'
git clone git://github.com/zsh-users/zsh-completions.git
rm -f ~/.zcompdump; compinit

echo 'setting up vim...' 
apt install vim -y
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
